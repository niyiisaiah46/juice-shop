# First stage: Install dependencies and prepare application
FROM node:18 as installer

# Set the working directory
WORKDIR /juice-shop

# Copy the application code
COPY . .

# Install global dependencies (TypeScript and ts-node)
RUN npm i -g typescript ts-node

# Install dependencies (excluding dev dependencies) with proper permissions
RUN npm install --production --unsafe-perm

# Perform cleanup
RUN npm dedupe \
    && rm -rf frontend/node_modules frontend/.angular frontend/src/assets \
    && mkdir logs \
    && chown -R 65532 logs \
    && chgrp -R 0 ftp/ frontend/dist/ logs/ data/ i18n/ \
    && chmod -R g=u ftp/ frontend/dist/ logs/ data/ i18n/ \
    && rm -f data/chatbot/botDefaultTrainingData.json ftp/legal.md i18n/*.json

# Second stage: Create a slim production image
FROM node:18-bookworm-slim

# Set the working directory
WORKDIR /juice-shop

# Copy the application files from the first stage
COPY --from=installer --chown=65532:0 /juice-shop .

# Set the user to a non-root user for security
USER 65532

# Expose the port the app runs on
EXPOSE 3000

# Command to run the application
CMD ["node", "build/app.js"]

