import requests
import sys

filename = sys.argv[1]
scan_type = ''

if filename == 'gitleaks.json':
    scan_type = 'Gitleaks Scan'
elif filename == 'njsscan.sarif':
    scan_type = 'SARIF'
elif filename == 'semgrep.json':
    scan_type = 'Semgrep JSON Report'
elif filename == 'retire.json':
    scan_type =  'Retire.js Scan'

api_key = '73d09e44dc7fad0ed110ecd078ace88b4ddf62c0'
engagement_id = 14

headers = {
    'Authorization': f'Token {api_key}'
}

url = 'https://demo.defectdojo.org/api/v2/import-scan'

data = {
    'active': True,
    'verified': True,
    'scan_type': scan_type,
    'minimum_severity': 'Low', 
    'engagement': engagement_id
}

with open(filename, 'rb') as file:
    files = {'file': file}
    response = requests.post(url, headers=headers, data=data, files=files)

if response.status_code == 201:
    print("Scan reports imported successfully!")
else: 
    print("Failed to import scan reports.")
